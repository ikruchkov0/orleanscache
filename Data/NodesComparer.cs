namespace OrleansCache.Data
{
    public class NodesComparer
    {
        private readonly Path _path;

        public NodesComparer(Path p)
        {
            _path = p;
        }

        public int Compare(Node i, Node j) => PairsNotInPath(i) < PairsNotInPath(j) ? -1 : 1;

        private int PairsNotInPath(Node node)
        {
            int count = 0;
            foreach (var n in node.Pairs())
            {
                if (_path.Contains(n.Value))
                {
                    continue;
                }

                count++;
            }

            return count;
        }
    }
}