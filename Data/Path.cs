using System;
using System.Collections.Generic;

namespace OrleansCache.Data
{
    public class Path
    {
        private PathNode _last;
        private readonly int _capacity;
        private readonly bool[] _attached;
        
        public Path(int capacity)
        {
            _capacity = capacity;
            Count = 0;
            _last = null;
            _attached = new bool[capacity + 1];
        }

        public bool Contains(int n)
        {
            return _attached[n];
        }

        public int Count { get; private set; }

        public void Push(int n)
        {
            if (_attached[n])
            {
                throw new Exception("Already attached");
            }

            var prev = _last;
            _last = new PathNode(n, prev);

            _attached[n] = true;
            Count++;
        }

        public void Pop()
        {
            if (_last == null)
            {
                return;
            }

            _attached[_last.Value] = false;
            var prev = _last.Prev;
            _last = prev;
            Count--;
        }

        public IReadOnlyList<int> ToReadOnlyList()
        {
            return ToVector(_last);
        }
        
        private int[] ToVector(PathNode node)
        {
            var v = new int[Count];
            var pos = 0;
            while (node != null)
            {
                v[pos] = node.Value;
                node = node.Prev;
                pos++;
            }

            return v;
        }
    }
}