namespace OrleansCache.Data
{
    public class PathNode
    {
        public PathNode(int n, PathNode prevNode)
        {
            Value = n;
            Prev = prevNode;
        }

        public int Value { get; }

        public PathNode Prev { get; }
    }
}