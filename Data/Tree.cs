using System;
using System.Collections.Generic;

namespace OrleansCache.Data
{
    public class Tree
    {
        private readonly Node[] _nodes;
        
        public Tree(int n)
        {
            _nodes = new Node[n];
        }

        public IReadOnlyList<Node> Roots => _nodes;

        public void AddPair(int head, int tail)
        {
            var headNode = GetOrCreate(head);
            var tailNode = GetOrCreate(tail);
            headNode.Add(tailNode);
        }

        public bool VerifyAllNodesHavePairs()
        {
            foreach (var n in _nodes)
            {
                if (n == null)
                {
                    return false;
                }

                if (n.PairsCount == 0)
                {
                    return false;
                }
            }

            return true;
        }

        public void SortPairs()
        {
            foreach (var n in _nodes)
            {
                n.SortPairs();
            }

            Array.Sort(_nodes, Node.Compare);
        }
        
        private Node GetOrCreate(int n)
        {
            var node = _nodes[n - 1];
            if (node == null)
            {
                node = new Node(n);
                _nodes[n - 1] = node;
            }

            return node;
        }
    }
}