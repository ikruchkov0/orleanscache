using System;
using System.Collections.Generic;
using System.Linq;

namespace OrleansCache.Data
{
    public static class SquareSumsCalculator
    {
        public static IReadOnlyList<int> SquareSumsRow(int n)
        {
            var tree = BuildTree(n);
            if (tree == null)
            {
                return Array.Empty<int>();
            }

            foreach (var root in tree.Roots)
            {
                var path = new Path(n);
                path.Push(root.Value);
                Dfs(n, root, path);
                if (path.Count == n)
                {
                    IReadOnlyList<int> result = path.ToReadOnlyList();
                    return result;
                }
            }

            return Array.Empty<int>();
        }
        
        private static bool IsFairSquare(int n)
        {
            var sqrtVal = Math.Sqrt(n);
            return Math.Abs(sqrtVal - Math.Floor(sqrtVal)) < double.Epsilon;
        }

        private static Tree BuildTree(int n)
        {
            var tree = new Tree(n);

            for (int i = 1; i <= n; i++)
            {
                for (int j = 1; j <= n; j++)
                {
                    if (i == j)
                    {
                        continue;
                    }

                    int sum = i + j;
                    if (!IsFairSquare(sum))
                    {
                        continue;
                    }

                    tree.AddPair(i, j);
                }
            }

            if (!tree.VerifyAllNodesHavePairs())
            {
                return null;
            }

            tree.SortPairs();
            return tree;
        }

        private static void Dfs(int n, Node node, Path path)
        {
            var comparer = new NodesComparer(path);
            var pairs = node.Pairs().Select(x => x).ToList();
            pairs.Sort((i, j) => comparer.Compare(i, j));

            foreach (var p in pairs)
            {
                int v = p.Value;

                if (path.Contains(v))
                {
                    continue;
                }

                path.Push(v);

                if (path.Count == n)
                {
                    return;
                }

                Dfs(n, p, path);
                if (path.Count == n)
                {
                    return;
                }

                path.Pop();
            }
        }
    }
}