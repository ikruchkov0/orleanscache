using System.Collections.Generic;

namespace OrleansCache.Data
{
    public class Node
    {
        private readonly List<Node> _pairs;

        public Node(int val)
        {
            Value = val;
            _pairs = new List<Node>();
        }

        public int Value { get; }

        public IReadOnlyList<Node> Pairs() => _pairs;

        public void SortPairs()
        {
            _pairs.Sort(Compare);
        }

        public void Add(Node node)
        {
            _pairs.Add(node);
        }

        public int PairsCount => _pairs.Count;
        
        public static int Compare(Node i, Node j) => i.PairsCount < j.PairsCount ? -1 : 1;
    }
}