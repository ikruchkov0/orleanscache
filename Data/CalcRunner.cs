using System.Threading.Tasks;

namespace OrleansCache.Data
{
    public class CalcRunner
    {
        private readonly TaskFactory _factory;

        public CalcRunner()
        {
            _factory = new TaskFactory(TaskScheduler.Default);
        }

        public Task<string> CalcAsync(int n)
        {
            return _factory.StartNew(() => Calc(n));
        }

        private string Calc(int n)
        {
            var list = SquareSumsCalculator.SquareSumsRow(n);
            return string.Join(", ", list);
        }
    }
}