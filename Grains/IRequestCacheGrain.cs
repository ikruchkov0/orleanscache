using System.Threading.Tasks;

namespace OrleansCache.Grains
{
    public interface IRequestCacheGrain: Orleans.IGrainWithStringKey
    {
        Task<string> GetAsync(int n);
    }
}