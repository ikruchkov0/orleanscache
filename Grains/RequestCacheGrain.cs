using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Orleans;
using OrleansCache.Data;

namespace OrleansCache.Grains
{
    public class RequestCacheGrain:  Orleans.Grain, IRequestCacheGrain
    {
        private TimeSpan _lifetime = TimeSpan.FromSeconds(10);
        private IDisposable _selfKillTimer;
        private readonly ILogger _logger;
        private readonly CalcRunner _calcRunner;
        private string? _response;

        public RequestCacheGrain(ILogger<RequestCacheGrain> logger, CalcRunner calcRunner)
        {
            _logger = logger;
            _calcRunner = calcRunner;
        }

        #region Overrides of Grain

        public override Task OnActivateAsync()
        {
            DelayDeactivation(_lifetime);
            _selfKillTimer = RegisterTimer( s =>
            {
                _logger.LogInformation($"Deactivation {this.GetPrimaryKeyString()}");
                DeactivateOnIdle();
                return Task.CompletedTask;
            }, null, _lifetime, _lifetime);
            return Task.CompletedTask;
        }

        #endregion

        #region Implementation of IRequestCacheGrain

        public async Task<string> GetAsync(int n)
        {
            if (_response != null)
            {
                _logger.LogInformation($"{this.GetPrimaryKeyString()} Response from cache");
                return _response;
            }

            var result = await _calcRunner.CalcAsync(n);
            _response = result;
            _logger.LogInformation($"{this.GetPrimaryKeyString()} Response from calculations");
            return _response;
        }

        #endregion
    }
}