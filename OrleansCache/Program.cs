using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Orleans.Hosting;
using Orleans;
using OrleansCache.Data;
using OrleansCache.Grains;

namespace OrleansCache
{
    public class Program
    {
        public static Task Main(string[] args) => CreateHostBuilder(args).Build().RunAsync();

        private static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                })
                .UseOrleans(siloBuilder =>
                {
                    siloBuilder
                        .UseLocalhostClustering()
                        .ConfigureApplicationParts(parts => parts
                            .AddApplicationPart(typeof(IRequestCacheGrain).Assembly).WithReferences()
                            .AddApplicationPart(typeof(RequestCacheGrain).Assembly).WithReferences());
                })
                .ConfigureServices(services => services.AddSingleton<CalcRunner>())
                .ConfigureLogging(logging => logging.AddConsole())
                .UseConsoleLifetime();
    }
}