using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Orleans;
using OrleansCache.Grains;

namespace OrleansCache
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    context.Response.ContentType = "text/html; charset=utf-8";
                    await context.Response.WriteAsync("<html><body> Check dev tools networks <script>for(var i = 0; i < 100; i++) { fetch('/calc?n=5000') }</script> </body></html>");
                });
            });
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/calc", async context =>
                {
                    int n;
                    if (!context.Request.Query.TryGetValue("n", out var strN) || !int.TryParse(strN, out n))
                    {
                        n = 25;
                    }
                    var clusterClient = context.RequestServices.GetService<IClusterClient>();

                    var requestCache = clusterClient.GetGrain<IRequestCacheGrain>($"Request{n}");

                    var res = await requestCache.GetAsync(n);
                    
                    await context.Response.WriteAsync(res);
                });
            });
        }
    }
}